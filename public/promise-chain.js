(function() {
	function log(msg) {
		const logText = document.getElementById("log").innerText;
        document.getElementById("log").innerText = logText + "\n" + msg;
		console.log(msg);
	}

	window.onload = function() {
		const startTime = new Date().getTime();
		setInterval(
			() => log("tick: " + (new Date().getTime() - startTime)),
			1000
		);

		const offlineCtx1 = new OfflineAudioContext(2, 44100, 44100);
		offlineCtx1.audioWorklet.addModule("processor.js")
			.then(() => log("first context loaded the processor") )
            .then(() => {
                const offlineCtx2 = new OfflineAudioContext(2, 44100, 44100);
                return offlineCtx2.audioWorklet.addModule("processor.js")
            })
			.then(() => log("second context loaded the processor") );

        log("promises have kicked off: this msg always shows up, and should show before any promises come back.");


	};
})();
