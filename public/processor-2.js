class Proc2 extends AudioWorkletProcessor {
    constructor() {
        super();
        this.port.onmessage = (event) => console.log("data in proc2 = " + event.data);
    }

    process(inputs, outputs, parameters) {
        return true;
    }
}

registerProcessor('proc2', Proc2);