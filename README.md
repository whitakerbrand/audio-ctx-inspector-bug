# audio-context-inspector-bug

This repository is an example of a Chrome bug that crashes the inspector when
loading multiple audio processors in WebAudio.